//
// Created by mitya on 25.11.2023.
//

#include "bmp.h"
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>



BMPFile* loadBMPFile(char* fname){
    if (access(fname, EACCES)){
        printf("Нет прав");
    }

    if (access(fname, ENOENT)){
        printf("Нет файла");
    }

    FILE* fp = fopen(fname, "rb");
    if (!fp){
        printf("Can't load");
        exit(0);
    };




    long lSize = ftell(fp);

    BMPFile* bmpFile = (BMPFile*) malloc(sizeof(BMPFile));
    size_t result = fread(&bmpFile->bhdr, sizeof(bmp_header), 1, fp);

    if (result != lSize){
        printf("Ошибка чтения");
    }

    //fread(bmpFile, sizeof(BMPFile), 1, fp);

    //int data_size = bmpFile->bhdr.biWidth*bmpFile->bhdr.biHeight*bmpFile->bhdr.biBitCount/8;
    //printf("%d\n", data_size);
    //printf("%d\n", bmpFile->bhdr.biSizeImage);
    bmpFile->data = (uint8_t * ) malloc(bmpFile->bhdr.biSizeImage + bmpFile->bhdr.biWidth*3);
    fseek(fp, bmpFile->bhdr.bOffBits, SEEK_SET);
    fread(bmpFile-> data, bmpFile->bhdr.biSizeImage, 1, fp);

    fclose(fp);
    return bmpFile;
}

void freeBMPFile(struct BMPFile* bmpFile){
    if (bmpFile){
        free(bmpFile);
    }
}
