
#include "bmp.h"
#include "pixel.h"
#include "rotate.h"
#include <stdio.h>
#include <stdlib.h>

#define NULLBYTE 0x00

void swap(size_t* a, size_t* b){
    size_t c = *a;
    *a = *b;
    *b = c;
}


int flagInit(int angel){
    angel = ((angel % 360) + 360) % 360;
    if (angel == 90){
        return 0;
    } else {
    if (angel == 270){
        return 0;
    } else {
    if (angel == 180){
        return 1;
    } else {
    if (angel == 0){
        return 1;
    } else {

        return 2;

    }}}

    }
}






pixelT* arrayInit( size_t size ) {
    pixelT* array = malloc(size * sizeof(pixelT));
    return array;
}

pixelT** marrayInit(size_t size, size_t row){
    pixelT** array = malloc(sizeof(pixelT*) * (row) * size);
    for (size_t i = 0; i < row; i = i + 1){
        array[i] = arrayInit(size);
    }
    return array;
}




void rotate2(BMPFile* bmpf, size_t size, size_t row, size_t sizeRowOfBytes, pixelT** pixels){
    size_t k = 0;
    for (size_t i = 0; i < size; i++) {
        for (size_t t = 0; t < row; t++) {
            k = i * sizeRowOfBytes + t * 3;
            if (size > 0 && row > 0) {
                bmpf->data[k] = pixels[i][t].b;
                bmpf->data[k + 1] = pixels[i][t].g;
                bmpf->data[k + 2] = pixels[i][t].r;
            }else{
                printf("Error");
            }
        }
        if ((4 - (row * 3) % 4) == 1) {
            bmpf->data[k + 3] = NULLBYTE;
        }
        if ((4 - (row * 3) % 4) == 2) {
            bmpf->data[k + 3] = NULLBYTE;
            bmpf->data[k + 4] = NULLBYTE;
        }
        if ((4 - (row * 3) % 4) == 3) {
            bmpf->data[k + 3] = NULLBYTE;
            bmpf->data[k + 4] = NULLBYTE;
            bmpf->data[k + 5] = NULLBYTE;
        }
    }

}

int main(int argc, char** argv ) {
    // (void) argc; (void) argv;
    char* inputFile = NULL;
    char* newfile = NULL;
    int angel = 0;
    if (argc == 4){
        inputFile = argv[1];
        newfile = argv[2];
        angel = atoi(argv[3]);

    } else{
        return 1;
    }
    //argc = 4;
    //argv[1] = "C:\\Users\\mitya\\CLionProjects\\final1\\assignment-3-image-rotation\\tester\\tests\\5\\input.bmp";
    //argv[2] = "C:\\Users\\mitya\\CLionProjects\\final1\\assignment-3-image-rotation\\solution\\img\\1.bmp";
    //argv[3] = "0";
    //inputFile = "C:\\Users\\mitya\\CLionProjects\\final1\\assignment-3-image-rotation\\tester\\tests\\3\\input.bmp";
    //newfile = "C:\\Users\\mitya\\CLionProjects\\final1\\assignment-3-image-rotation\\solution\\img\\1.bmp";
    //angel = 270;


    BMPFile* bmpf = loadBMPFile(inputFile);

    size_t size =  10;
    size_t row = 10;

    size = bmpf->bhdr.biWidth;
    row = bmpf->bhdr.biHeight;


    pixelT** pixels = malloc((sizeof(pixelT*) * row * size));
    if (row > 0) {
        for (int i = 0; i < row; i++) {
            pixelT *array = malloc((sizeof(pixelT) * size));

            pixels[i] = array;
        }
    } else{

        free(pixels);
        return 1;
    }



    int j = 0;


    size_t sizeCountOfBytes = (4 - (size*3)%4) + size*3;
    size_t sizeRowOfBytes = (4 - (row*3)%4) + row*3;



    pixelT myPixel;
    size_t r = 0;


    if (r < (size_t)bmpf->bhdr.biSizeImage) {
        while (r < (size_t) bmpf->bhdr.biSizeImage) {
            //for (uint32_t i = 0; i < bmpf->bhdr.biSizeImage ; i = i + 3){
            if ((!(((r % sizeCountOfBytes) + 2) > (size * 3))) && j < row) {
                myPixel.b = bmpf->data[r];
                myPixel.g = bmpf->data[r + 1];
                myPixel.r = bmpf->data[r + 2];
                size_t k = (r % (size_t) sizeCountOfBytes) / 3;
                pixels[j][k] = myPixel;
                r = r + 3;
            } else {
                j = j + 1;
                r = r + (4 - (size * 3) % 4);
            }
        }
    } else{
        free(pixels);
        return 1;
    }



    int flag = flagInit(angel);
    if (angel == 90 || angel == -270){
        pixels = rotateLeft(pixels, size, row);
        pixels = rotateLeft(pixels, row, size);
        pixels = rotateLeft(pixels, size, row);
    } else {
    if (angel == -90 || angel == 270){
        pixels = rotateLeft(pixels, size, row);
    } else {
    if (angel == 180 || angel == -180){
        pixels = rotateLeft(pixels, size, row);
        pixels = rotateLeft(pixels, row, size);
    } else {
    if (angel == 0){
    } else{
        free(pixels);
        return 1;
    }
    }}}




if (flagInit(angel) == 2){

    free(pixels);
    return 1;
}


    if (flag == 1) {
        swap(&size, &row);
        rotate2(bmpf, size, row, sizeCountOfBytes, pixels);
        swap(&size, &row);
    } else {
        rotate2(bmpf, size, row, sizeRowOfBytes, pixels);

        uint32_t b = bmpf->bhdr.biHeight;
        bmpf->bhdr.biHeight = bmpf->bhdr.biWidth;
        bmpf->bhdr.biWidth = b;

        bmpf->bhdr.biSizeImage =  size * sizeRowOfBytes * 1;
    }


    for (size_t i = 0; i < bmpf->bhdr.biHeight; i++){
        free(pixels[i]);
    }
    free(pixels);

    FILE *file = fopen(newfile, "wb");
    fwrite(&bmpf->bhdr, sizeof(bmpf->bhdr), 1, file);



    printf("%d\n", bmpf->bhdr.biBitCount);

    fwrite(bmpf->data, bmpf->bhdr.biSizeImage, 1, file);
    free(bmpf->data);
    fclose(file);

    freeBMPFile(bmpf);


    return 0;
}






