//
// Created by mitya on 25.11.2023.
//

#include "bmp.h"
#include "pixel.h"
#include <stdio.h>
#include <stdlib.h>


void array_reverse(struct pixel* array, size_t size) {
    if (size != 0){
        for (size_t i = 0; i < size/2; i = i + 1){
            struct pixel x = array[i];
            array[i] = array[size-i - 1];
            array[size-i - 1] = x;
        }
    }
}

struct pixel* arrayInit1( size_t size, size_t row ) {
    struct pixel* array = malloc(row * size*sizeof(struct pixel));
    //array_int_fill(array, *size);
    return array;
}

struct pixel** marrayInit1(size_t size, size_t row){
    struct pixel** array = malloc(sizeof(struct pixel*) * (row));
    for (size_t i = 0; i < row; i = i + 1){
        array[i] = arrayInit1(row, size);
    }
    return array;
}

void array_int_print1(struct pixel* array, size_t size){
    for (size_t i = 0; i < size; i++){
        printf("%hhu ", array[i].b);
        printf("%hhu ", array[i].g);
        printf("%hhu ", array[i].r);
    }
    printf("\n");
}


pixelT** rotateLeft(pixelT** marray, size_t size, size_t rows){
    pixelT** newMarray = (pixelT **) malloc((sizeof(pixelT *) * size) + 1000);
    for (int i = 0; i < size; i++){
        newMarray[i] = (pixelT *) malloc((sizeof (pixelT) * rows) + 1000);
    }
    for (size_t i = 0; i < size; i++){
        for (int j = 0; j < rows; j++) {
            newMarray[i][j] = marray[rows - j - 1][i];
        }
    }

    /**struct pixel* array;
    for (size_t i = 0; i < rows; i++){
        array = marray[i];
        //array_int_print1(array, 2);
        array_reverse(array, size);
        //array_int_print1(array, 2);
        for (size_t j = 0; j < size; j++){
            newMarray[j][i] = array[j];
            //printf("%lld\n", marray[i][j]);
        }

    }
     */


    for (size_t i = 0; i < rows; i++){
        free(marray[i]);
    }
    free(marray);

    return newMarray;
}










