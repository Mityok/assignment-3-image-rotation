//
// Created by mitya on 28.12.2023.
//

#ifndef IMAGE_TRANSFORMER_PIXEL_H
#define IMAGE_TRANSFORMER_PIXEL_H
#include  <stdint.h>

typedef struct pixel{
    uint8_t b, g, r;

} pixelT;


#endif //IMAGE_TRANSFORMER_PIXEL_H
