//
// Created by mitya on 25.11.2023.
//

#ifndef IMAGE_TRANSFORMER_BMP_H
#define IMAGE_TRANSFORMER_BMP_H

//#include "pixel.h"
#include  <stdint.h>

#pragma pack(push, 1)
typedef struct bmp_header
{
    // BMP Header
    uint16_t bfType;
    uint32_t  bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;

    //DIB Header
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t  biHeight;
    uint16_t  biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t  biClrImportant;
} bmp_header;

typedef struct BMPFile{
     bmp_header bhdr;
     uint8_t*  data;
} BMPFile;
#pragma pack(pop)

BMPFile* loadBMPFile(char* fname);
void freeBMPFile(BMPFile* bmpFile);




#endif //IMAGE_TRANSFORMER_BMP_H

