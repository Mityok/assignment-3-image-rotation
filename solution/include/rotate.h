//
// Created by mitya on 25.11.2023.
//


#include <stddef.h>
#include <stdint.h>
#ifndef IMAGE_TRANSFORMER_ROTATE_H
#define IMAGE_TRANSFORMER_ROTATE_H
struct pixel** rotateLeft(pixelT** marray, size_t size, size_t rows);
#endif //IMAGE_TRANSFORMER_ROTATE_H
